package pages;

import core.Core;
import org.openqa.selenium.By;

public class ItemPage {
    private final Core core;

    private static final By ADD_TO_BAG = By.id("aAddToBag");
    private static final By FIRST_AVAILABLE_SIZE = By.cssSelector("[class='tooltip sizeButtonli']");

    public ItemPage(Core core) {
        this.core = core;
    }

    public void addItemToBag() {
        core.findPresentElement(ADD_TO_BAG).click();
    }

    public void selectFirstAvailableSize() {
        core.findPresentElement(FIRST_AVAILABLE_SIZE).click();
    }
}
