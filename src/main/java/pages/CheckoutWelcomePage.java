package pages;

import core.Core;
import org.openqa.selenium.By;

public class CheckoutWelcomePage {
    private final Core core;

    private static final By GUEST_EMAIL_FIELD = By.xpath("//*[@id=\"Guest_EmailAddress\"]");
    private static final By CONTINUE_BUTTON = By.xpath("//*[@id=\"continue\"]/div/div[2]");

    public CheckoutWelcomePage(Core core) {
        this.core = core;
    }

    public void fillGuestEmailField() {
        core.findPresentElement(GUEST_EMAIL_FIELD).sendKeys("test@test.com");
    }

    public void clickContinueButton() {
        core.findPresentElement(CONTINUE_BUTTON).click();
    }
}
