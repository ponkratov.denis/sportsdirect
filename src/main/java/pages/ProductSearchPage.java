package pages;

import core.Core;
import org.openqa.selenium.By;

public class ProductSearchPage {
    private Core core;

    private static final By FIRST_ITEM = By.xpath("//*[@id=\"navlist\"]/li[1]");

    public ProductSearchPage(Core core) {
        this.core = core;
    }

    public void selectFirstItem() {
        core.findPresentElement(FIRST_ITEM).click();
    }
}
