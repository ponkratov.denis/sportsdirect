package pages;

import core.Core;
import org.openqa.selenium.By;

public class MainPage {
    private final Core core;

    private static final String MAIN_PAGE_URL = "https://lv.sportsdirect.com/";
    private static final By MENS_CATEGORY = By.xpath("//a[@href='/mens']");
    private static final By MENS_BOOTS_CATEGORY = By.xpath("//a[@href='/mens/footwear/boots']");
    private static final By MY_BAG_BUTTON = By.id("bagQuantityContainer");

    public MainPage(Core core) {
        this.core = core;
    }

    public void open() {
        core.open(MAIN_PAGE_URL);
    }

    public void openMensBootsCategory() {
        core.moveTo(MENS_CATEGORY);
        core.findPresentElement(MENS_BOOTS_CATEGORY).click();
    }

    public void clickBagButton() {
        core.findPresentElement(MY_BAG_BUTTON).click();
    }
}
