package pages;

import core.Core;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

public class CheckoutDeliveryPage {
    private final Core core;

    private static final By ENTER_ADDRESS_MANUALLY_BUTTON = By.className("manuallyAddAddress");
    private static final By FIRST_NAME_FIELD = By.xpath("//*[@id=\"NewAddressSelection_Address_FirstName\"]");
    private static final By LAST_NAME_FIELD = By.xpath("//*[@id=\"NewAddressSelection_Address_Surname\"]");
    private static final By COUNTRY_FIELD = By.xpath("//*[@id=\"ddlCountry\"]");
    private static final By ADDRESS_LINE1_FIELD = By.xpath("//*[@id=\"NewAddressSelection_Address_Line1\"]");
    private static final By ADDRESS_LINE2_FIELD = By.xpath("//*[@id=\"NewAddressSelection_Address_Line2\"]");
    private static final By TOWN_NAME_FIELD = By.xpath("//*[@id=\"NewAddressSelection_Address_Town\"]");
    private static final By REGION_NAME_FIELD = By.xpath("//*[@id=\"NewAddressSelection_Address_RegionName\"]");
    private static final By POSTCODE_FIELD = By.xpath("//*[@id=\"NewAddressSelection_Address_Postcode\"]");
    private static final By TELEPHONE_NUMBER_FIELD = By.xpath("//*[@id=\"TelephoneNumber\"]");
    private static final By CONTINUE_BUTTON = By.cssSelector("div[class='ProgressButContain col-xs-12 hidden-xs hidden-sm']");
    private static final By SECOND_CONTINUE_BUTTON = By.className("ContinueOn");

    public CheckoutDeliveryPage(Core core) {
        this.core = core;
    }

    //Click "Enter address manually"
    public void clickEnterAddressManually() {
        core.findPresentElement(ENTER_ADDRESS_MANUALLY_BUTTON).click();
    }

    //Click on the "First name" field and input an first name
    public void fillFirstName() {
        core.findPresentElement(FIRST_NAME_FIELD).sendKeys("John");
    }

    //Click on the "Last name" field and input last name
    public void fillLastName() {
        core.findPresentElement(LAST_NAME_FIELD).sendKeys("Doe");
    }

    //Click on the "Country" dropdown field and select the country
    public void selectCountry() {
        Select country = new Select(core.findPresentElement(COUNTRY_FIELD));
        country.selectByValue("LV");
    }

    //Click on the "Address" line 1 field and input an address line 1
    public void fillAddressLine1() {
        core.findPresentElement(ADDRESS_LINE1_FIELD).sendKeys("Skanstes iela 52");
    }

    //Click on the "Address" line 2 field and input an address line 2
    public void fillAddressLine2() {
        core.findPresentElement(ADDRESS_LINE2_FIELD).sendKeys("777");
    }

    //Click on the "Town / City" field and input an town/city
    public void fillTown() {
        core.findPresentElement(TOWN_NAME_FIELD).sendKeys("Riga");
    }

    //Click on the "County / State" field and input an county/state
    public void fillCounty() {
        core.findPresentElement(REGION_NAME_FIELD).sendKeys("Latvia");
    }

    //Input a postcode in to the "Postcode / Zip" field
    public void fillPostcode() {
        core.findPresentElement(POSTCODE_FIELD).sendKeys("LV-1013");
    }

    //Input a contact number in to the "Contact number" field
    public void fillContactNumber() {
        core.findPresentElement(TELEPHONE_NUMBER_FIELD).sendKeys("+37120012345");
    }

    //Click on the first "Continue" button
    public void clickFirstContinueButton() {
        core.findPresentElement(CONTINUE_BUTTON).click();
    }

    //Click on the second "Continue" button
    public void clickSecondContinueButton() {
        core.findElements(SECOND_CONTINUE_BUTTON).get(6).click();
    }
}
