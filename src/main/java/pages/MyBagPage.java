package pages;

import core.Core;
import org.openqa.selenium.By;

public class MyBagPage {
    private final Core core;

    private final static By CONTINUE_BUTTON = By.id("divContinueSecurely");

    public MyBagPage(Core core) {
        this.core = core;
    }

    public void clickContinueButton() {
        core.findPresentElement(CONTINUE_BUTTON).click();
    }
}
