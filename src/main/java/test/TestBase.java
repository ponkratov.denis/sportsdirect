package test;

import core.Core;
import org.junit.After;

public class TestBase {
    protected Core core = new Core();

    @After
    public void afterEachTest() {
        core.quit();
    }
}
