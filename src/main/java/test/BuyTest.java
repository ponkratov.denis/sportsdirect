package test;

import org.junit.Test;
import pages.*;

public class BuyTest extends TestBase {
    private final MainPage mainPage = new MainPage(core);
    private final ProductSearchPage productSearchPage = new ProductSearchPage(core);
    private final ItemPage itemPage = new ItemPage(core);
    private final MyBagPage myBagPage = new MyBagPage(core);
    private final CheckoutWelcomePage checkoutWelcomePage = new CheckoutWelcomePage(core);
    private final CheckoutDeliveryPage checkoutDeliveryPagePage = new CheckoutDeliveryPage(core);

    @Test
    public void buyItemTest() {
        mainPage.open();
        mainPage.openMensBootsCategory();
        productSearchPage.selectFirstItem();
        itemPage.selectFirstAvailableSize();
        itemPage.addItemToBag();
        mainPage.clickBagButton();
        myBagPage.clickContinueButton();
        checkoutWelcomePage.fillGuestEmailField();
        checkoutWelcomePage.clickContinueButton();
        checkoutDeliveryPagePage.clickEnterAddressManually();
        checkoutDeliveryPagePage.fillFirstName();
        checkoutDeliveryPagePage.fillLastName();
        checkoutDeliveryPagePage.selectCountry();
        checkoutDeliveryPagePage.fillAddressLine1();
        checkoutDeliveryPagePage.fillAddressLine2();
        checkoutDeliveryPagePage.fillTown();
        checkoutDeliveryPagePage.fillCounty();
        checkoutDeliveryPagePage.fillPostcode();
        checkoutDeliveryPagePage.fillContactNumber();
        checkoutDeliveryPagePage.clickFirstContinueButton();
        checkoutDeliveryPagePage.clickSecondContinueButton();
    }
}
