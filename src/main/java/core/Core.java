package core;

import lombok.extern.java.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

@Log
public class Core {
    private WebDriver driver;
    private static final String CHROME_DRIVER_LOCATION = "./src/main/resources/drivers/chromedriver";
    private static final int WAIT_TIME = 10;

    public Core() {
        System.setProperty("webdriver.chrome.driver", CHROME_DRIVER_LOCATION);
        this.driver = new ChromeDriver();
    }

    public void open(String url) {
        if (!url.contains("http://") && !url.contains("https://")) {
            url = "http://" + url;
        }
        log.info("Open url: " + url);
        driver.get(url);
    }

    public WebElement findPresentElement(By element) {
        log.info("Find element: " + element);
        return new WebDriverWait(driver, Duration.ofSeconds(WAIT_TIME))
                .until(ExpectedConditions.presenceOfElementLocated(element));
    }

    public WebElement findClickableElement(By element) {
        log.info("Find element: " + element);
        return new WebDriverWait(driver, Duration.ofSeconds(WAIT_TIME))
                .until(ExpectedConditions.elementToBeClickable(element));
    }

    public List<WebElement> findElements(By element) {
        return driver.findElements(element);
    }

    public void moveTo(By locator) {
        log.info("Move to locator: " + locator);
        Actions actions = new Actions(driver);
        WebElement element;
        element = (new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.presenceOfElementLocated(locator)));
        actions.moveToElement(element).perform();
        sleepFor(1000);
    }

    public void sleepFor(Integer sleepMillis) {
        log.info("Sleep for: " + sleepMillis);
        try {
            Thread.sleep(sleepMillis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void quit() {
        log.info("Quit driver");
        driver.quit();
    }
}
